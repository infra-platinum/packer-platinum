packer {
  required_plugins {
    hcloud = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/hcloud"
    }
  }
}

locals {
  name = "nomad"
  version = "21Q4"
  ripe    = "bronze"
}

source "hcloud" "cfg" {
  image        = "ubuntu-20.04"
  location     = "nbg1"
  server_type  = "cpx11"
  ssh_username = "root"
}

build {
  source "hcloud.cfg" {
    server_name   = "${local.name}-builder"
    snapshot_name = "${local.name}-${local.version}-${local.ripe}"
    snapshot_labels = {
      name       = local.name
      ripe       = local.ripe
      version    = local.version
      created_by = "packer"
    }
  }
  provisioner "shell" {
    inline = ["apt update -y && apt install -y ansible"]
  }
  provisioner "ansible-local" {
    playbook_file = "./nomad-playbook.yml"
  }
  provisioner "shell" {
    inline = ["nomad version"]
  }
  provisioner "file" {
    source = "nomad.unit"
    destination = "/usr/lib/systemd/system/nomad.unit"
  }
  provisioner "shell" {
    inline = ["systemctl enable nomad"]
  }
}
