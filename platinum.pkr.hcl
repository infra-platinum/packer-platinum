packer {
  required_plugins {
    hcloud = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/hcloud"
    }
  }
}

locals {
  name = "platinum"
  version = "21Q4"
  ripe    = "coal"
}

source "hcloud" "cfg" {
  image_filter {
    with_selector = [
      "name==nomad",
      "version==21Q4",
      "ripe==bronze",
    ]
  }
  location     = "nbg1"
  server_type  = "cpx11"
  ssh_username = "root"
}

build {
  source "hcloud.cfg" {
    server_name   = "${local.name}-builder"
    snapshot_name = "${local.name}-${local.version}-${local.ripe}"
    snapshot_labels = {
      name       = local.name
      ripe       = local.ripe
      version    = local.version
      created_by = "packer"
    }
  }
}
